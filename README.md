### HMAC

Easily generate Authorization headers or send signed requests!

###### Installation

`go get gitlab.com/drpebcak/hmac`

Binary releases are also available!

